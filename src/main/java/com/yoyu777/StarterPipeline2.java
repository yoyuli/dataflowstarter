/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.yoyu777;

import com.google.api.services.bigquery.model.Table;
import com.google.api.services.bigquery.model.TableRow;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.coders.Coder;
import org.apache.beam.sdk.coders.CoderException;
import org.apache.beam.sdk.io.FileIO;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.*;
import org.apache.beam.sdk.values.PCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ucar.ma2.Array;
import ucar.ma2.DataType;
import ucar.ma2.IndexIterator;
import ucar.ma2.InvalidRangeException;
import ucar.nc2.NetcdfFile;
import ucar.nc2.Variable;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;
import java.util.List;

/**
 * A starter example for writing Beam programs.
 *
 * <p>The example takes two strings, converts them to their upper-case
 * representation and logs them.
 *
 * <p>To run this starter example locally using DirectRunner, just
 * execute it without any additional parameters from your favorite development
 * environment.
 *
 * <p>To run this starter example using managed resource in Google Cloud
 * Platform, you should specify the following command-line options:
 * --project=<YOUR_PROJECT_ID>
 * --stagingLocation=<STAGING_LOCATION_IN_CLOUD_STORAGE>
 * --runner=DataflowRunner
 */
public class StarterPipeline2 {
    private static final Logger LOG = LoggerFactory.getLogger(StarterPipeline2.class);

    public static class ArrayCoder extends Coder<Array> {
        @Override
        public void encode(Array value, OutputStream outStream) throws CoderException, IOException {
            ByteBuffer bb=value.getDataAsByteBuffer();
            WritableByteChannel channel = Channels.newChannel(outStream);
            channel.write(bb);
        }

        @Override
        public Array decode(InputStream inStream) throws CoderException, IOException {
            byte[] ba = new byte[inStream.available()];
            inStream.read(ba);
            ByteBuffer bb=ByteBuffer.wrap(ba);
            Array a=Array.factory(DataType.getType("FLOAT"),new int[]{18,1,960,1280},bb);
            return a;
        }

        @Override
        public List<? extends Coder<?>> getCoderArguments() {
            return null;
        }

        @Override
        public void verifyDeterministic() throws NonDeterministicException {

        }
    }


    public static class readBytes extends SimpleFunction<FileIO.ReadableFile,Array> {
        @Override
        public Array apply(FileIO.ReadableFile input){
            try {
                byte[] data = input.readFullyAsBytes();
                NetcdfFile ncFile=NetcdfFile.openInMemory("file",data);
                Variable v=ncFile.findVariable("moisture_content_of_soil_layer");
                Array a=  v.read(":,0,:,:")
                        .permute(new int[]{3,2,1,0});
                return a;
            }catch (IOException e){

            }catch (InvalidRangeException e){

            }
            return null;
        }
    }


    public static class tranformFile extends PTransform<PCollection<Array>, PCollection<TableRow>> {
        @Override
        public PCollection<TableRow> expand(PCollection<Array> input) {
            PCollection<TableRow> rows=input.apply(ParDo.of(new readData()));
            return rows;
        }

        private class readData extends DoFn<Array, TableRow>{
            @ProcessElement
            public void processElement(ProcessContext c) {
               IndexIterator iter=c.element().getIndexIterator();
                while (iter.hasNext()) {
                    double val = iter.getFloatNext();
                    TableRow row=new TableRow()
                            .set("x", iter.getCurrentCounter()[0])
                            .set("y", iter.getCurrentCounter()[1])
                            .set("z", iter.getCurrentCounter()[2])
                            .set("value",val);
                    c.output(row);
                }
            }

        }
    }

    public static void main(String[] args) {
        Pipeline p = Pipeline.create(
                PipelineOptionsFactory.fromArgs(args).withValidation().create());

        p.apply(FileIO.match().filepattern("/Users/yoyuli/Downloads/20180111T1500Z-PT0003H-soil_mass_concentration_of_water_on_soil_levels.nc"))
                .apply(FileIO.readMatches())
                .apply(MapElements.via(new readBytes())).setCoder(new ArrayCoder())
                .apply(new tranformFile());

//        POutput output= p.apply(Create.of("/Users/yoyuli/Downloads/prods_op_mogreps-uk_20130114_03_00_003.nc")
//                                    .withType(TypeDescriptors.strings()))
//                .apply(MapElements.via(new readNCFile())).setCoder();

        p.run();
        LOG.info("OK");
    }
}
