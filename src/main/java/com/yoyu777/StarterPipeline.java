/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.yoyu777;


import com.google.api.services.bigquery.model.TableFieldSchema;
import com.google.api.services.bigquery.model.TableRow;
import com.google.api.services.bigquery.model.TableSchema;

import org.apache.beam.runners.dataflow.DataflowRunner;
import org.apache.beam.runners.dataflow.options.DataflowPipelineOptions;

import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.FileIO;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.*;
import org.apache.beam.sdk.values.KV;
import org.apache.beam.sdk.values.PCollection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ucar.ma2.Array;
import ucar.ma2.InvalidRangeException;
import ucar.nc2.NetcdfFile;
import ucar.nc2.Variable;

import java.io.IOException;
import java.util.*;
import java.util.stream.IntStream;

/**
 * A starter example for writing Beam programs.
 *
 * <p>The example takes two strings, converts them to their upper-case
 * representation and logs them.
 *
 * <p>To run this starter example locally using DirectRunner, just
 * execute it without any additional parameters from your favorite development
 * environment.
 *
 * <p>To run this starter example using managed resource in Google Cloud
 * Platform, you should specify the following command-line options:
 * --project=<YOUR_PROJECT_ID>
 * --stagingLocation=<STAGING_LOCATION_IN_CLOUD_STORAGE>
 * --runner=DataflowRunner
 */
public class StarterPipeline {
    private static final Logger LOG = LoggerFactory.getLogger(StarterPipeline.class);

    public static class readBytes extends SimpleFunction<FileIO.ReadableFile,float[]> {
        @Override
        public float[] apply(FileIO.ReadableFile input){
            try {
                byte[] data = input.readFullyAsBytes();
                NetcdfFile ncFile=NetcdfFile.openInMemory("file",data);
                Variable v=ncFile.findVariable("cloud_volume_fraction_in_atmosphere_layer");
                Array a= v.read(":,0,:,:");
                float[] ja= (float[]) a.getStorage();
                int[] s=a.getShape();
                // shape: {18,1,960,1280}
                return ja;
            }catch (IOException e){

            }catch (InvalidRangeException e){

            }
            return null;
        }

    }

    public static class constructTableRow extends PTransform<PCollection<float[]>, PCollection<TableRow>>{
        @Override
        public PCollection<TableRow> expand(PCollection<float[]> input) {
                PCollection<TableRow> rows=input.apply(ParDo.of(new split()));
                return rows;
        }

        private class split extends DoFn<float[],TableRow>{
            @ProcessElement
            public void processElement(ProcessContext c) {
                int[] shape={18,1,960,1280};
                int L=c.element().length;
                IntStream.range(0, L)
                        .forEach(i -> {
                            TableRow row = new TableRow();
                            row.set("dim3", i % shape[3]);
                            row.set("dim2", (int) Math.floor(i / shape[3]) % shape[2]);
                            row.set("dim1", (int) Math.floor(i / (shape[3] * shape[2])) % shape[1]);
                            row.set("dim0", (int) Math.floor(i / (shape[3] * shape[2] * shape[1])) % shape[0]);
                            row.set("value", c.element()[i]);
                            c.output(row);
                        });
            }

        }
    }


    public static void main(String[] args) {
        // Create and set your PipelineOptions.
        DataflowPipelineOptions options = PipelineOptionsFactory.as(DataflowPipelineOptions.class);

        // For Cloud execution, set the Cloud Platform project, staging location,
        // and specify DataflowRunner.
        options.setProject("cr-celab");
        options.setStagingLocation("gs://sci-data/binaries");
        options.setGcpTempLocation("gs://sci-data/gcpTemp");
        options.setTempLocation("gs://sci-data/temp");
        options.setNumWorkers(2);
        options.setMaxNumWorkers(8);
        options.setRegion("europe-west1");
        options.setRunner(DataflowRunner.class);
        Pipeline p = Pipeline.create(options);

        List<TableFieldSchema> fields = new ArrayList<>();
        fields.add(new TableFieldSchema().setName("dim3").setType("INTEGER"));
        fields.add(new TableFieldSchema().setName("dim2").setType("INTEGER"));
        fields.add(new TableFieldSchema().setName("dim1").setType("INTEGER"));
        fields.add(new TableFieldSchema().setName("dim0").setType("INTEGER"));
        fields.add(new TableFieldSchema().setName("value").setType("FLOAT"));
        TableSchema schema=new TableSchema().setFields(fields);

        p.apply(FileIO.match().filepattern("gs://sci-data/h5repacked-cloud_amount_on_height_levels.nc" ))
//        p.apply(FileIO.match().filepattern("/Users/yoyuli/Downloads/h5repacked-cloud_amount_on_height_levels.nc" ))
                .apply(FileIO.readMatches())
                .apply(MapElements.via(new readBytes()))
                .apply(new constructTableRow())
                .apply(BigQueryIO.writeTableRows()
                        .to("cr-celab:scidata.test")  // a table specification in the form of "[project_id]:[dataset_id].[table_id]"
                        .withSchema(schema)
                );

        p.run();
        LOG.info("OK");
    }
}
